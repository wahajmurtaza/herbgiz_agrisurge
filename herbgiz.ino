#include <DHT.h>
#include <custom_functions.h>

//eeprom
#include <Arduino.h>
#include <E24.h>
#define temp_addr 0x10
#define h_addr  0x11
#define light_addr 0x12
#define count1_addr 0x13
#define count2_addr 0x14
#define day_addr 0x15


//clock library
#include <Time.h>
#include <TimeLib.h>
#include <Wire.h>
#include <DS1307RTC.h>


//plant pins
#define ac_pin 12
#define heater_pin 11
#define fan_pin 10

#define motor_up 7
#define motor_down 6
#define motor_enable 5

#define lim_switch 4
#define echoPin  2
#define pingPin  3

#define pump 9

#define light A0



//DHT 22 sensor
#define DHTPIN 8 
#define DHTTYPE    DHT22


//initialize
DHT dht(DHTPIN, DHTTYPE);
tmElements_t tm;
E24 e24 = E24(E24Size_t::E24_512K, E24_DEFAULT_ADDR);


//define control data
float desired_temp=0.0;
float desired_humidity=0.0;
int desired_distance=22;               //centimeters
int desired_light=0;


//define read only data
float humidity=0.0,stemperature=0.0;              //dht values read only
int hours=0,minutes=0,seconds=0,days=0;                    //clock
int distance=0;                     //centimeters
bool heater,ac,fan;                   //read only 
String inputstr,newstr;
int minute_count=0;
int last_day=0;
unsigned int start_count=0;


//define functions
void read_dht();
void print_dht();
void read_clock();
void print_clock();
void maintain_temperature();            
void measure_distance();
void maintain_humidity();
void set_height();
void circulate();
void linker();
void printserial();
void setpoint_change();
void maintain_light();

unsigned int current_time2, last_time2;






//******************************************************************************************************************************
void setup()
{
  int m,r;

  pinMode(motor_down,OUTPUT);
  pinMode(motor_enable,OUTPUT);
  pinMode(motor_up,OUTPUT);
  pinMode(A0,OUTPUT);
  pinMode(ac_pin,OUTPUT);
  pinMode(heater_pin,OUTPUT);
  pinMode(fan_pin, OUTPUT);
  pinMode(echoPin,INPUT);
  pinMode(pingPin,OUTPUT);
  pinMode(pump,OUTPUT);

  //Serial
  Serial.begin(9600);

  //clock sync
  setSyncProvider(RTC.get);

  //DHT SETUP
  dht.begin();

  desired_temp = e24.read(temp_addr);
  desired_humidity= e24.read(h_addr);
  desired_light= e24.read(light_addr);
  
  
  m= e24.read(count1_addr);
  r= e24.read(count2_addr);
  minute_count=(m*255)+r;

  last_day=e24.read(day_addr);
  
  //minute_count=0;last_day=0;              //uncomment for zeroing
  /*
  Serial.println(desired_temp);
  Serial.println(desired_humidity);
  Serial.println(desired_light);
  Serial.println(minute_count);
  Serial.println(last_day);
  */

  
  


}


//*****************************************************************************************************************************************

//-----------------------------------------------------------------------------------------------------------------------------------------
void loop() 
{
  start_count=millis();
  //start_count=40000;   //test
  delay(1000);
  read_dht();
  //print_dht();
  read_clock();
  
  
  if(start_count>5000)
  {
  maintain_temperature();
  maintain_humidity();
  maintain_light();
  measure_distance();
  set_height();
  circulate();
  }
  
  linker();
  
  
}

//-----------------------------------------------------------------------------------------------------------------------------------------
void read_dht()
  {
    static int current_time4,last_time4;
    current_time4=millis();
    if(last_time4>current_time4)last_time4=0;
    if(current_time4-last_time4>2050)
    {
      humidity = dht.readHumidity();
      stemperature = dht.readTemperature();
      last_time4=current_time4;
    }
  }

void print_dht()
  {
    Serial.print("t = ");
    Serial.print(stemperature);
    Serial.print("   h = ");
    Serial.println(humidity);
  }

void read_clock()
  {
  custom.read(tm);
  }

void print_clock()
  {
    Serial.print(hours);
    Serial.print(":");
    Serial.print(minutes);
    Serial.print(":");
    Serial.println(seconds);
  
  }

void maintain_temperature()
  {
    static int current_minutes,last_minutes;
    static bool working;
    //print_dht();
    //Serial.println(last_minutes);
    //Serial.println(current_minutes);
    //read_dht();
  read_clock();
    
    current_minutes=seconds;                     //set to minutes  =minutes
    if(current_minutes<5 && last_minutes>0)          // set to 15 
    {
      last_minutes=last_minutes-60;
    }
    if(current_minutes>last_minutes+5)
    {
      //Serial.println(last_minutes);
        if(stemperature>desired_temp+3 && working==false && heater==false)  //temp low than desired
        {
          digitalWrite(ac_pin,HIGH);  //turn ac on
          working=true;
          ac=true;
          
        }
    
        else if(working==true && stemperature<=desired_temp-3 && heater==false)
        {
          digitalWrite(ac_pin,LOW);
          working=false;
          ac=false;
          last_minutes=second();  
          
          
        }
    
        else if(stemperature<desired_temp-3 && working==false && ac==false)
        {
          digitalWrite(heater_pin,HIGH);
          heater=true;
          working=true;
        }
    
        else if(working==true && stemperature>=desired_temp+3 && ac==false)
        {
          digitalWrite(heater_pin,LOW);
          working=false;
          heater=false;
          last_minutes=second();
        }
    }
          
  }

void measure_distance()
  {
    //Serial.print("distance= ");
    //distance=ultrasonic.distanceInMillimeters();
    //Serial.println(distance);
    long duration;
    static int last_distance;
    digitalWrite(pingPin, LOW);
    delayMicroseconds(2);
    digitalWrite(pingPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(pingPin, LOW);
    duration = pulseIn(echoPin, HIGH); 

    distance = duration / 29 / 2; // cal
    if(distance<=4 || distance>50)distance=last_distance; 
    last_distance=distance;
    
   // Serial.print(distance);
    //Serial.println("cm");
  }

void maintain_humidity()
  {
    if(humidity>desired_humidity+5)
    {
      digitalWrite(fan_pin,HIGH);
      fan=true;
    }
    if(humidity<desired_humidity-5)
    {
      digitalWrite(fan_pin,LOW);
      fan=false;
    }
  }

void set_height()
  {
    
    if(minutes==1)                //set to if(hours==0 && minutes==0)
      {
        measure_distance();
        float error=abs(desired_distance-distance);
        const int kp=2;
        int motor_speed=constrain(error*kp,0,255);
        
        if(distance>desired_distance+1)
        {
          digitalWrite(motor_up,LOW);
          digitalWrite(motor_down,HIGH);
          //Serial.println("going down");
        }
        if(distance<desired_distance-1)
        {
          digitalWrite(motor_up,HIGH);
          digitalWrite(motor_down,LOW);
         // Serial.println("going up");
          if(digitalRead(lim_switch)==HIGH || distance==desired_distance)
          {
            digitalWrite(motor_down,LOW);
            digitalWrite(motor_up,LOW);
          }
        }
        if(distance==desired_distance)
        {
          digitalWrite(motor_down,LOW);
          digitalWrite(motor_up,LOW); 
         // Serial.println("Sopped");
        }
        
        
          analogWrite(motor_enable,255);
      }
    else
      digitalWrite(motor_enable,LOW);
  }
           
void circulate()
  {
    //if(hours%4==0 && minutes >0 && minutes <15)
    //Serial.println(String("remainder    ") + minutes%5);
    
    if(minutes%5==0)
    {
      //Serial.println("pump high");
      digitalWrite(pump,HIGH);
    }
    else 
    {
     // Serial.println("pump high");
      digitalWrite(pump,LOW);
    }
  }

void linker()
{
  static char c;
  if(Serial.available()>0)
  {
    
  
    c=Serial.read();
    if(c=='a')
    {
    setpoint_change(inputstr);
    newstr=inputstr;
    inputstr="";
    //runtest=1;
    }
    else
    {
    inputstr+=c;
    }
    
  }
  
  current_time2=millis();
  if(current_time2-last_time2>1000)
  {
     printserial();
    last_time2=current_time2;
  }
  

}



void printserial()
{
  Serial.print(stemperature,0);
  Serial.print(" , ");
  Serial.print(humidity,0);
  Serial.print(" , ");
  if(ac==false)Serial.print("0 , ");
  if(ac==true)Serial.print("1 , ");
  if(heater==false)Serial.print("0 , ");
  if(heater==true)Serial.print("1 , ");
  if(fan==false)Serial.print("0 , ");
  if(fan==true)Serial.print("1 , ");
  Serial.println(constrain((desired_light*60)-minute_count,0,1440));
 
}

void setpoint_change(String pars)
{
  int stemp,shum,slight;
  String str1,str2,str3;
  str1=pars.substring(0,pars.indexOf(","));
  str2=pars.substring(pars.indexOf(",")+1,pars.indexOf("*"));
  str3=pars.substring(pars.indexOf("*")+1);
  stemp=str1.toInt();
  shum=str2.toInt();
  slight=str3.toInt();
  if(stemp!=desired_temp)
  {
    e24.write(temp_addr,stemp);
    desired_temp=stemp;
    //Serial.print("changing t");
  }
  if(shum!=desired_humidity)
  {
    e24.write(h_addr,shum);
    desired_humidity=shum;
    //Serial.println("changing h");
  }
  if(slight!=desired_light)
  {
    e24.write(light_addr,slight);
    desired_light=slight;
    //Serial.println("changing l");
  }
}

void maintain_light()
  {
    static int last_minute3,current_minute3;
    int multiple,remainder;
    current_minute3=minutes;
    if(last_minute3!=current_minute3 && (desired_light*60)-minute_count>0 &&  (desired_light*60)-minute_count<1000)
    {
      minute_count++;
      last_minute3=current_minute3;
      multiple=minute_count/255;
      remainder=minute_count%255;
      /*
      Serial.print(multiple);
      Serial.print("        ");
      Serial.print(remainder);
      Serial.print("   ");
      Serial.println((multiple*255)+remainder);
      */
      e24.write(count1_addr,multiple);
      e24.write(count2_addr,remainder);
    }
    
  if(minute_count<desired_light*60)
  {
    //lstate=true;
    digitalWrite(light,HIGH);
  }
  if(minute_count>=desired_light*60)
  {
    //lstate=false;
    digitalWrite(light,LOW);
  }
  
  if(last_day!=days)
  {
    minute_count=0;
   Serial.print(days);
   last_day=days;
   e24.write(day_addr,days);
  }
  
  }
